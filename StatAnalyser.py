from math import sqrt
class Analyser:
    """Analyser Class"""
    def __init__(self,data,funk):
        """class constructor data is a list of x and y floats and funk is fitted
        function."""
        self.funk=funk
        self.datax=data[0]
        self.datay=data[1]
    def std_dev_calc(self):
        """calc standtart deviation"""
        sum=0
        for el in self.datay:
            sum=sum+el
        self.avg=sum/len(self.datay)
        sum=0
        for el in self.datay:
            sum=sum+(el-self.avg)**2
        self.std_dev=sqrt(sum/(len(self.datay)-1))
    def pearson(self):
        """run pearson test"""
        self.std_dev_calc()
        sum=0
        for i in range(len(self.datax)):
            sum=sum+(( self.datay[i]-self.funk(self.datax[i]) )/self.std_dev)**2
        self.result=sum
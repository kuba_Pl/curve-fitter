
from ttk import *
from Tkinter import *
import tkMessageBox
import tkFileDialog
import tkSimpleDialog
import pickle
import os

from data_generator import *
from config import *
from fitter import *
from function_generator import funk_creator
from StatAnalyser import Analyser

from matplotlib.figure import Figure
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg

fit_names=("sinusoid","logarithmic","exponential","polynomial")
analysis_names=("formula found: ","chi2 test: ","normalised: ","standart deviation:  ")


class GUI(Frame):
    """GUI class"""
    def __init__(self, parent):
        """GUI constructor"""
        Frame.__init__(self, parent)   
        self.parent = parent
        self.initUI()
        
        self.samples=samples
        self.noise=noise
        self.xo=xo
        self.xk=xk
        self.distrib=distrib
        
    def initUI(self):
        """initialize GUI parts"""
        self.parent.title("CurveFitter")
        self.pack(fill=BOTH, expand=1)
        for i in range(0,13):
            self.columnconfigure(i, pad=4)
        for i in range(0,13):
            self.rowconfigure(i,pad=4)
        self.fit_par_widget()
        self.menu_widget()
        self.plot_widget()
        self.plot_analys_widget()
    def fit_par_widget(self):
        labels=[]
        space=Label(self,text="")
        space.grid(row=0,column=0,rowspan=2,columnspan=4)
        space2=Label(self,text="")
        space2.grid(row=0,column=5,columnspan=4)
        for i in range(len(fit_names)):
            labels.append(Label(self,text=fit_names[i]))
            labels[i].grid(row=i+2,column=0)
        
        self.checkbtns=[]
        bools=[]
        for i in range(len(fit_names)):
            bools.append(BooleanVar())
            self.checkbtns.append([Checkbutton(self,variable=bools[i],
            command=self.bool_command),bools[i]])
            self.checkbtns[i][0].grid(row=i+2,column=1,sticky="W")
            self.checkbtns[i][0].deselect()
        self.checkbtns[3][0].config(text="degree")
        self.last=Checkbutton(self)
        
        self.entry=[]
        pdeg=StringVar()
        self.entry.append([Entry(self,textvariable=pdeg),pdeg])
        self.entry[0][0].grid(column=3,row=5)
        self.entry[0][0].config(width=2)
        button=Button(self,command=self.plot,text="fit")
        button.grid(column=0,row=6,columnspan=3)
    def menu_widget(self):
        menubar = Menu(self.parent)
        self.parent.config(menu=menubar)
        loadMenu=Menu(menubar)
        loadMenu.add_command(label="Load...", command=self.load)
        menubar.add_cascade(label="Load data", menu=loadMenu)
        genMenu=Menu(menubar)
        genMenu.add_command(label="Change sampling",command=self.sampling)
        genMenu.add_command(label="Change noise",command=self.ch_noise)
        genMenu.add_command(label="Change noise distrib",command=self.ch_distrib)
        genMenu.add_command(label="Change x0",command=self.ch_x0)
        genMenu.add_command(label="Change xk",command=self.ch_xk)
        genMenu.add_command(label="Generate", command=self.generate)
        menubar.add_cascade(label="Generator", menu=genMenu)
        fileMenu = Menu(menubar)
        fileMenu.add_command(label="Exit", command=self.exit)
        menubar.add_cascade(label="Exit", menu=fileMenu)
    def plot_widget(self):
        f = Figure(figsize=(3, 2), dpi=100)
        self.canvas = FigureCanvasTkAgg(f, master=self)
        self.plot_handler = f.add_subplot(111)
        self.canvas.get_tk_widget().grid(column=5,row=2,rowspan=6,columnspan=2)
        self.canvas.show()
    def plot_analys_widget(self):
        labels=[]
        for i in range(len(analysis_names)):
            labels.append(Label(self,text=analysis_names[i]))
            labels[i].grid(row=i+9,column=5,sticky='W')
        self.analis=[]
        var=[]
        for i in range(len(analysis_names)):
            var.append(StringVar())
            self.analis.append([Label(self,textvariable=var[i]),var[i]])
            self.analis[i][0].grid(row=i+9,column=6,columnspan=1,sticky='E')
    def bool_command(self):
        """associated with checkbuttons-only one must be true """
        self.last.deselect()
        for i in range(len(fit_names)):
            if self.checkbtns[i][1].get()==True:
                self.last=self.checkbtns[i][0]
                if i == 3:
                    self.entry[0][0].config(state="normal")
                else:
                    self.entry[0][0].config(state="disabled")

        self.last.select()
    def exit(self):
        """close GUI"""
        self.quit()
    def load(self):
        """load data from path read from config"""
        ftypes = [('Pickle','*.p'),('Python files', '*.py'), ('All files', '*')]
        dlg = tkFileDialog.Open(self, filetypes = ftypes)
        fl = dlg.show()
        if fl != '':
            lists = []
            file = open(fl, 'rb')
            while 1:
                try:
                    lists=(pickle.load(file))
                except EOFError:
                    break
            file.close()
            tmp=[]
            for el in lists:
                if el != 'y':
                    tmp.append(el)
                else:
                    self.x=tmp
                    tmp=[]
                    continue
            self.y=tmp
            self.plot_handler.clear()
            self.plot_handler.plot(self.x,self.y,'ro')
            self.canvas.show()
    def generate(self):
        """generate data"""
        st=tkSimpleDialog.askstring("generator","write a function")
        if st ==None:return
        try:
            if self.xo > self.xk:
                tmp=self.xo
                self.xo=self.xk
                self.xk=tmp
            tmp=generator(self.noise,st,[self.xo,self.xk,self.samples],self.distrib)
            tmp.generate()
            if not os.path.exists(save_path):
                os.makedirs(save_path)
            save=save_path+'/'+st+str(self.xo)+'_'+str(self.xk)+'_'+str(self.samples)+'_'+str(self.noise)+'.p'
            with open(save, 'ab') as f:
                lis=list(tmp.rang)+["y"]+tmp.y
                pickle.dump(lis, f)
            f.close()
            return
        except SyntaxError:
            self.generate()
            return
    def plot(self):
        """plot data"""
        try:
            for i in range(len(self.checkbtns)+1):
                if self.checkbtns[i][1].get() == True:break
            if i == 4:return
            if i == 3 and self.entry[0][1].get() == "":return
            if i == 3 and str(self.entry[0][1].get())<1:return
            if i == 0:tmp=fitter_function(3,type="sin")
            if i == 1:tmp=fitter_function(3,type="log")
            if i == 2:tmp=fitter_function(3,type="exp")
            if i == 3:tmp=fitter_function(int(self.entry[0][1].get()),type="poly")
        except ValueError:
            tkMessageBox.showinfo("ValueError","Cant convert degree to int.")
            return
        fit=fitter(tmp.f,np.asarray(self.x),np.asarray(self.y))
        try:
            fit.fit()
        except RuntimeError:
            tkMessageBox.showinfo("RuntimeError","Unable to fit parameters.")
            return
        f=funk_creator(i,fit.popt)
        xe=self.x
        ye=[]
        for x in xe:
          ye.append(f.f(x))
        self.plot_handler.clear()
        self.plot_handler.plot(self.x,self.y,'ro')
        self.plot_handler.plot(xe,ye,'b')
        self.canvas.show()
        anal=Analyser([self.x,self.y],f.f)
        anal.pearson()
        self.analis[0][1].set(f.tf)
        self.analis[1][1].set(str(anal.result)[0:6])
        print(len(self.x))
        self.analis[2][1].set(str(anal.result/(len(self.x)-1))[0:6])
        self.analis[3][1].set(str(anal.std_dev)[0:6])
        print(fit.popt)
    def sampling(self):
        """change sampling"""
        st=tkSimpleDialog.askstring("generator","Edit number of samples.")
        if st ==None:return
        try:
            self.samples=int(st)
            if self.samples < 1:raise ValueError
        except ValueError:
            self.sampling()
            return
    def ch_noise(self):
        """change noise parameter"""
        st=tkSimpleDialog.askstring("generator","Edit noise parameter")
        if st ==None:return
        try:
            self.noise=int(st)
            if self.noise < 0:self.noise=0
        except ValueError:
            self.ch_noise()
            return
    def ch_x0(self):
        """change begining of function"""
        st=tkSimpleDialog.askstring("generator","Edit x0")
        if st ==None:return
        try:
            self.x0=int(st)
        except ValueError:
            self.ch_x0()
            return
    def ch_xk(self):
        """change end of function"""
        st=tkSimpleDialog.askstring("generator","Edit xk")
        if st ==None:return
        try:
            self.xk=int(st)
        except ValueError:
            self.ch_xk()
            return
    def ch_distrib(self):
        """change distrib"""
        st=tkSimpleDialog.askstring("generator","Edit distrib")
        if st ==None:return
        try:
            tmp=generator(0,0,[0,0,0],distrib=st)
            if tmp.dst==None:raise ValueError
        except ValueError:
            self.ch_distrib()
            return
def main():
    """main function"""
    root = Tk()
    ex = GUI(root)
    root.geometry("600x400+300+300")
    root.mainloop()  


import numpy as np
class funk_creator:
    """generates funtion based on type and parameters"""
    def __init__(self,type,par):
        """type is a type of the function, par is a list of fitted parameters"""
        self.par=par
        self.type=type
        if type==0:f=self.sinus_funk_creator()
        if type==1:f=self.log_funk_creator()
        if type==2:f=self.exp_funk_creator()
        if type==3:f=self.poly_funk_creator()
        self.f=f
    def sinus_funk_creator(self):
        """generate sinus"""
        par=self.par
        p0=par[0]
        p1=par[1]
        p2=par[2]
        f=lambda x: p0*np.sin(x+p1)+p2
        self.tf=str(p0)[0:4]+"*sin(x+"+str(p1)[0:4]+")+"+str(p2)[0:4]
        return f
    def log_funk_creator(self):
        """generate logarthim"""
        par=self.par
        p0=par[0]
        p1=par[1]
        p2=par[2]
        f=lambda x: p0*np.log(x+p1)+p2
        self.tf=str(p0)[0:4]+"*log(x+"+str(p1)[0:4]+")+"+str(p2)[0:4]
        return f
    def exp_funk_creator(self):
        """generate exponential"""
        par=self.par
        p0=par[0]
        p1=par[1]
        p2=par[2]
        f=lambda x: p0*np.exp(x+p1)+p2
        self.tf=str(p0)[0:4]+"*exp(x*"+str(p1)[0:4]+")+"+str(p2)[0:4]
        return f
    def poly_funk_creator(self):
        """generate polynomial"""
        global par
        par=self.par
        text="def funk(x):return par["+str(0)+"]"
        for i in range(1,len(self.par)):
            text=text+"+par["+str(i)+"]"+"*x**"+str(i)
        exec(text)
        f=funk
        self.tf=str(self.par[0])[0:4]
        for i in range(1,len(self.par)):
            self.tf=self.tf+"+"+str(self.par[i])[0:4]+"*x**"+str(i)
        return f
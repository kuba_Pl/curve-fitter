from math import *
import numpy as np
from scipy.optimize import curve_fit
class fitter_function:
    """class for generating funcitob for fit"""
    def __init__(self,number=2,type="poly"):
        """number is used only in polynomial,else its ommited. type is a type of
        function which user want to fit"""
        self.number=number+1
        self.type=type
        if self.type == "poly":
            self.generate_text()
            self.text_to_code()
        else:
            self.string=None
        if self.type=="sin":
            def f(x,p0,p1,p2): return p0*np.sin(x+p1)+p2 
            self.f=f
        if self.type=="exp":
            def f(x,p0,p1,p2): return p0*np.exp(p1*x)+p2
            self.f=f
        if self.type=="log":
            def f(x,p0,p1,p2): return p0*np.log(x+p1)+p2
            self.f=f
    def generate_text(self):       
        """text generating for polynomial input"""
        self.__string="def f(x"
        for i in range(0,self.number):
            self.__string=self.__string+",p"+str(i)
        self.__string=self.__string+"):return p0"
        for i in range(1,self.number):
            self.__string=self.__string+"+p"+str(i)+"*x**"+str(i)
    def text_to_code(self):
        """compile the created text"""
        if self.__string==None:
            return
        exec(self.__string)
        self.f=f
    
    
class fitter:
    """fitting function class"""
    def __init__(self,f,rang,funk):
        """f is funtion which user want to fit,rang is list of xvalues,funk is list of
        """
        self.f=f
        self.funk=funk
        self.rang=rang
    def fit(self):
        """run fitting based on scipy curve_fit"""
        self.popt,self.pcov =curve_fit(self.f,self.rang,self.funk)
    
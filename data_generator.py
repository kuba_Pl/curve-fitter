
import numpy as np
from math import *
import parser

class generator:
    """class for generating y values based on input"""
    def __init__(self,noise,funk,interv,distrib="normal"):
        """constructor for generator noise is noise level,funk is a function which
        user want to generate,interv is a list of interval based on formula [xo,xk,
        samples],distribution is distribution on which is based generate of the noise,
        default is normal"""
        
        self.noise=noise
        self.funk=funk
        self.rang=np.linspace(interv[0],interv[1],interv[2])
        self.dst=distrib
        self.distrib_rec()
    def distrib_rec(self):
        """parsing the input and adding proper distribution"""
        if self.dst == "normal":
            self.dst=np.random.normal
            return
        if self.dst == "flat":
            def f (ign,x):return (2*np.random.random()-1)*x
            self.dst=f
            return
        if self.dst=="None":
            def f(ign,x):return 0
            self.dst=f
            return
        self.dst=None
    def generate(self):
        """run generator method"""
        funk=[]
        code = parser.expr(self.funk).compile()
        for x in self.rang:
            funk.append(eval(code)+self.dst(0,self.noise))
        self.y=funk